package ru.rencredit.jschool.kuzyushin.tm.constant;

public interface ITerminalConst {

    String CMD_HELP = "help";

    String CMD_VERSION = "version";

    String CMD_ABOUT = "about";

    String CMD_EXIT = "exit";
}
