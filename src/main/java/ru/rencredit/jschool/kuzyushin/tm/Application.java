package ru.rencredit.jschool.kuzyushin.tm;

import ru.rencredit.jschool.kuzyushin.tm.constant.ITerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ITerminalConst.CMD_VERSION:
                showVersion();
                break;
            case ITerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case ITerminalConst.CMD_HELP:
                showHelp();
                break;
            case ITerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                showErrorMessage(arg);
        }
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void showErrorMessage(final String arg) {
        System.out.println("'" + arg + "' is not a Task Manager command. See 'help'");
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Alexey Kuzyushin");
        System.out.println("E-MAIL: alexeykuzyushin@yandex.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(ITerminalConst.CMD_ABOUT + " - Show developer info.");
        System.out.println(ITerminalConst.CMD_VERSION + " - Show version info.");
        System.out.println(ITerminalConst.CMD_HELP + " - Display terminal commands.");
        System.out.println(ITerminalConst.CMD_EXIT + " - Close program");
    }
}
